# Improbable Test Project

__by Stephen Chatelain__

This project was written in PHP 7, Javascript, HTML, and CSS. It uses no frameworks and has no dependencies.

PHP was selected for its rapid development time, and because it's the language I'm most proficient with for web development.

## Servers

This project consists of two parts: frontend and backend. They are completely decoupled and require two web servers to function.

You can run the project using the built-in PHP development server by running the following commands in their respective directories:

    /frontend$> php -S localhost:8000

    /backend$> php -S localhost:9000

The frontend port is arbitrary, but the backend port is hard-coded to __9000__ in /frontend/script.js, so the server must be set on that port to function.

You should now be able to navigate to *http://localhost:8000* in a browser, which will call the API on port __9000__.

## CLI

The API can also be used from the command line.

To get the projects:

    /$>  curl -X GET http://localhost:9000/projects

To get deployments for a project, use */deployments/[project name]*:

    /$>  curl -X GET http://localhost:9000/deployments/lofi

## Testing

There is a simple test script that can test the database method calls and the routing. Run it from the command line inside of the */src/tests* directory with:

    /tests$> php -c ./custom.ini test.php

The custom.ini file is needed to enable assertions in PHP 7.
