// URL of the API. If you run the api on a different port, you can specify that here.
var apiUrl = "http://localhost:9000/";

/**
 * Retrieve all projects from API
 * and create DOM elements to display
 */
function loadProjects()
{
    // clear old data
    var container = document.getElementById("container");
    var msg = document.getElementById("message");
    msg.style.display = "block";

    var xhttp = new XMLHttpRequest();
    xhttp.timeout = 5000; // time in milliseconds
    xhttp.addEventListener("error", () =>document.getElementById("message").innerHTML = "Error loading projects");
    xhttp.addEventListener("abort", () => document.getElementById("message").innerHTML = "Loading projects aborted");
    xhttp.addEventListener("timeout", () => document.getElementById("message").innerHTML = "Request taking too long");

    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            msg.style.display = 'none';
            document.getElementById("projects").style.display = 'block';

            resp = JSON.parse(this.responseText);
            console.log(resp);
            var projectsList = document.getElementById('projects-list');
            for (var i = 0; i < resp.length; i++) {
                var obj = resp[i];
                var li = document.createElement("li");
                li.setAttribute("id", obj.projectName);
                li.setAttribute("onclick", "loadDeployments('" + obj.projectName + "')");
                li.innerHTML = obj.projectName;
                projectsList.appendChild(li);
            }
        } else {
            document.getElementById("message").innerHTML = "Loading...";
        }

    };
    xhttp.open("GET", apiUrl + "projects", true);
    xhttp.send();
}

/**
 * Retrieve all deployments for the given project
 * and create DOM elements to display
 *
 * @param {string} project The name of the project to query
 */
function loadDeployments(project)
{
    // Re-write url to reflect current project for deep-linking
    window.history.pushState('Deployments', project, '/' + project);

    // clear old data
    var container = document.getElementById("container");
    var projects = document.getElementById("projects");
    projects.style.display = "none";
    var msg = document.getElementById("message");
    msg.style.display = "block";

    var xhttp = new XMLHttpRequest();
    xhttp.addEventListener("error", () => document.getElementById("message").innerHTML = "Error loading projects");
    xhttp.addEventListener("abort", () => document.getElementById("message").innerHTML = "Loading projects aborted");
    xhttp.addEventListener("timeout", () => document.getElementById("message").innerHTML = "Request taking too long");

    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            msg.style.display = 'none';
            document.getElementById("deployments").style.display = 'block';
            document.getElementById("project-name").innerHTML = project;

            resp = JSON.parse(this.responseText);
            var deploymentsList = document.getElementById('deployments-list');

            // RUNNING
            if (typeof resp['running'] !== 'undefined') {
                var running = resp['running'];
                for (var i = 0; i < running.length; i++) {
                    var obj = running[i];
                    var li = document.createElement("li");
                    li.innerHTML = obj.deploymentName + " (Running)";
                    deploymentsList.appendChild(li);
                }
            }
            // STOPPED
            if (typeof resp['stopped'] !== 'undefined') {
                var stopped = resp['stopped'];
                for (var i = 0; i < stopped.length; i++) {
                    var obj = stopped[i];
                    var li = document.createElement("li");
                    li.innerHTML = obj.deploymentName + " (Stopped)";
                    deploymentsList.appendChild(li);
                }
            }
        } else {
            document.getElementById("message").innerHTML = "Loading...";
        }
    };
    xhttp.open("GET", apiUrl + "deployments/" + project, true);
    xhttp.send();
}
