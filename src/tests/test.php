<?php

// Set the current working directory to /backend so the csv file paths work
chdir('../backend/');

require_once 'Router.php';
require_once 'Database.php';

$database = new Database;

// set routes
Router::route('/projects', function() use ($database) {
    return $database->getProjects();
});

Router::route('/deployments', function($projects) use ($database) {
    return [
        'running' => $database->getDeploymentsRunning($projects),
        'stopped' => $database->getDeploymentsStopped($projects)
    ];
});

echo "Running tests...\n";

// Test database methods
assert($database->getProjects()[0]['projectName'] == 'lofi');
assert($database->getDeploymentsRunning('lofi')[0]['deploymentName'] == 'alpha-test-121');
assert($database->getDeploymentsStopped('lofi')[0]['deploymentName'] == 'alpha-test-160');

// Test routing dispatch for project
$action = 'projects';
$response = Router::dispatch($action);
assert($response[0]['projectName'] == 'lofi');

// Test routing dispatch for deployment
$action = 'deployments/lofi';
$response = Router::dispatch($action);
assert($response['running'][0]['deploymentName'] == 'alpha-test-121');

echo "All tests complete\n";
