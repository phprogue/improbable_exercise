<?php
/**
 * Creates routes and dispatches requests to those routes
 * Does not validate request methods.
 */
class Router
{
    private static $routes = [];

    /**
     * Register a new route
     *
     * @param string    $action
     * @param $callback Closure
     */
    public static function route(string $action, Closure $callback)
    {
        $action = trim($action, '/');
        self::$routes[$action] = $callback;
    }

    /**
     * Dispatch the router
     *
     * @param string $action
     */
    public static function dispatch(string $action)
    {
        $args = explode('/', trim($action, '/'));
        $project = '';
        if ($args[0] == 'deployments') {
            if (count($args) < 2) {
                return ['error' => 'Bad route'];
            }
            $project = $args[1];
        }
        $path = $args[0];
        $callback = self::$routes[$path];

        return call_user_func($callback, $project);
    }
}
