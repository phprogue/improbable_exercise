<?php

require_once 'Router.php';
require_once 'Database.php';

// Artificial slow-down to allow frontend to display loading state.
// This would be removed in production.
sleep(1);

$database = new Database;

// set routes
Router::route('/projects', function() use ($database) {
    return $database->getProjects();
});

Router::route('/deployments', function($projects) use ($database) {
    return [
        'running' => $database->getDeploymentsRunning($projects),
        'stopped' => $database->getDeploymentsStopped($projects)
    ];
});

$action = $_SERVER['REQUEST_URI'];
$response = Router::dispatch($action);

// send response to client
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
echo json_encode($response);
