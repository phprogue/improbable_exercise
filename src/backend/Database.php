<?php

/**
 * Handles all reading and parsing of CSV files
 */
class Database
{
    const PROJECTS_FILE = 'csv/projects.csv';
    const DEPLOYMENTS_RUNNING_FILE = 'csv/deployments-running.csv';
    const DEPLOYMENTS_STOPPED_FILE = 'csv/deployments-stopped.csv';

    private $projects;
    private $deploymentsRunning;
    private $deploymentsStopped;
    private $deploymentsRunningColumns;
    private $deploymentsStoppedColumns;

    public function __construct()
    {
        $this->projects = [];
        $this->deploymentsRunning = [];
        $this->deploymentsStopped = [];
        $this->deploymentsRunningColumns = [];
        $this->deploymentsStoppedColumns = [];
    }

    /**
     * Parse csv file and return all projects
     *
     * @return array
     */
    public function getProjects(): array
    {
        $this->projects = $this->readFile(self::PROJECTS_FILE);

        return $this->projects;
    }

    /**
     * Find and return all running deployments for specified project
     *
     * @param  string $project Project name
     * @return array Returns running deployments or empty array if none exist
     */
    public function getDeploymentsRunning(string $project): array
    {
        $this->deploymentsRunning = $this->readFile(self::DEPLOYMENTS_RUNNING_FILE);
        $running = [];

        foreach ($this->deploymentsRunning as $dr) {
            if ($dr['projectName'] == $project) {
                $running[] = $dr;
            }
        }
        return $running;
    }

    /**
     * Find and return all stopped deployments for specified project
     *
     * @param  string $project Project name
     * @return array Returns stopped deployments or empty array if none exist
     */
    public function getDeploymentsStopped(string $project): array
    {
        $this->deploymentsStopped = $this->readFile(self::DEPLOYMENTS_STOPPED_FILE);
        $stopped = [];

        foreach ($this->deploymentsStopped as $ds) {
            if ($ds['projectName'] == $project) {
                $stopped[] = $ds;
            }
        }

        return $stopped;
    }

    /**
     * Parse csv and return associative array (hash map) of data
     * with csv headers as array keys
     *
     * @param  string $filename File to parse
     * @return array
     */
    private function readFile(string $filename): array
    {
        if (!$filename) {
            throw new Exception("Missing filename");
        }

        if (false === ($file = file($filename, FILE_IGNORE_NEW_LINES))) {
            error_log("Cannot read from file: " . $filename);
            return [];
        }

        $rows = array_map('str_getcsv', $file);
        $header = array_shift($rows);
        $arr = [];
        foreach ($rows as $row) {
            $arr[] = array_combine($header, $row);
        }

        return $arr;
    }
}
